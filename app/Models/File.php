<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class File extends Model
{
    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string[]
     *
     */
    protected $fillable = ['id', 'title', 'description', 'url', 'name'];

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $table = 'files';

    public function __construct(array $attributes = [])
    {
        $this->id = (string)Uuid::uuid4();

        $attributes['id'] = $this->id;

        parent::__construct($attributes);
    }

}
