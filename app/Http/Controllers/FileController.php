<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helper\File\Interfaces\FileHelperInterface;
use App\Repositories\Interfaces\FileRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class FileController
 */
final class FileController extends Controller
{
    /**
     * @var \App\Helper\File\Interfaces\FileHelperInterface
     */
    private $fileHelper;

    /**
     * @var \App\Repositories\Interfaces\FileRepositoryInterface
     */
    private $fileRepo;

    /**
     * FileController constructor.
     *
     * @param \App\Repositories\Interfaces\FileRepositoryInterface $fileRepository
     * @param \App\Helper\File\Interfaces\FileHelperInterface $fileHelper
     */
    public function __construct(FileRepositoryInterface $fileRepository, FileHelperInterface $fileHelper)
    {
        $this->fileRepo = $fileRepository;
        $this->fileHelper = $fileHelper;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     *
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $data = $request->toArray();

        $request->validate(FileRepositoryInterface::CREATE_RULES);

        if (in_array($data['encodedFile']['ext'], FileHelperInterface::TYPES) === false) {
            return response('Invalid file type. Must be MP4 or JPG only', 422);
        }

        try {
            $result = $this->fileHelper->upload($data['encodedFile'] ?? []);

            $data['url'] = $result['url'];
            $data['name'] = $result['name'];
            $file = $this->fileRepo->create($data);

            return response($file, 200);
        } catch (\Exception $exception) {
            return response($exception->getMessage(), 400);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $data = $request->toArray();

        $params = [
            'page' => $data['page'] ?? 1,
            'itemsPerPage' => $data['itemsPerPage'] ?? 5
        ];

        $files = $this->fileRepo->paginate($params);

        return response($files, 200);
    }

    /**
     * @param string $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $file = $this->fileRepo->getById($id);

        if ($file === null) {
            return response('File does not exist.', 404);
        }

        $request->validate(FileRepositoryInterface::UPDATE_RULES);

        $data = $request->toArray();

        if ($data['file'] !== null) {
            if (in_array($data['encodedFile']['ext'], FileHelperInterface::TYPES) === false) {
                return response('Invalid file type. Must be MP4 or JPG only', 422);
            }

            $this->fileHelper->remove($file);

            $result = $this->fileHelper->upload($data['encodedFile'] ?? []);

            $data['url'] = $result['url'];
            $data['name'] = $result['name'];
        }

        $file = $this->fileRepo->update($id, $data);

        return response($file, 200);
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function view(string $id)
    {
        $file = $this->fileRepo->getById($id);

        if ($file === null) {
            return response('File does not exist.', 404);
        }

        return response($file, 200);
    }
}
