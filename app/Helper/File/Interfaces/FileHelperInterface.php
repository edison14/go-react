<?php
declare(strict_types=1);

namespace App\Helper\File\Interfaces;

use App\Models\File;

interface FileHelperInterface
{
    public const TYPES = [
        self::TYPE_IMAGE,
        self::TYPE_VIDEO
    ];
    public const TYPE_IMAGE = 'jpg';
    public const TYPE_VIDEO = 'mp4';

    /**
     * @param $file
     *
     * @return void
     */
    public function remove(File $file): void;

    /**
     * @param $image
     *
     * @return mixed[]
     */
    public function upload($image): array;
}
