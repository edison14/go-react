<?php
declare(strict_types=1);

namespace App\Helper\File;

use App\Helper\File\Interfaces\FileHelperInterface;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

final class FileHelper implements FileHelperInterface
{
    public const IMAGE_PATH = 'images';

    public const VIDEO_PATH = 'videos';

    public function remove(File $file): void
    {
        $ext = pathinfo($file->url, PATHINFO_EXTENSION);

        $folder = $ext === FileHelperInterface::TYPE_VIDEO
            ? self::VIDEO_PATH
            : self::IMAGE_PATH;

        Storage::disk(sprintf('public_%s', $folder))->delete($file->name);
    }

    /**
     * @param $image
     *
     * @return mixed[]
     */
    public function upload($image): array
    {
        $folder = $image['ext'] === FileHelperInterface::TYPE_VIDEO
            ? self::VIDEO_PATH
            : self::IMAGE_PATH;

        $fileName = sprintf('%s.%s', Uuid::uuid4()->toString(), $image['ext']);
        $data = substr($image['encoded'], strpos($image['encoded'], ',') + 1);

        Storage::disk(sprintf('public_%s', $folder))->put($fileName, base64_decode($data));

        return [
            'url' => sprintf('%s/storage/%s/%s', env('APP_URL'), $folder, $fileName),
            'name' => $fileName
        ];
    }
}
