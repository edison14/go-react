<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\File;
use App\Repositories\Interfaces\FileRepositoryInterface;

final class FileRepository implements FileRepositoryInterface
{
    /**
     * @param mixed[] $params
     *
     * @return \App\Models\File
     */
    public function create($params): File
    {
        $file = new File($params);

        $file->save();

        return $file;
    }

    /**
     * @param string $id
     *
     * @return null|\App\Models\File
     */
    public function getById(string $id): ?File
    {
        return File::find($id);
    }

    /**
     * @param mixed[] $params
     *
     * @return object
     */
    public function paginate(array $params): object
    {
        return File::orderBy('updated_at', 'DESC')
            ->paginate($params['itemsPerPage'], $columns = ['*'], $pageName = 'page', $params['page']);
    }

    /**
     * @param string $id
     * @param array $params
     *
     * @return \App\Models\File
     */
    public function update(string $id, array $params): File
    {
        $file = File::find($id);

        $file->fill($params);

        $file->save();

        return $file;
    }
}
