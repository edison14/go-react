<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

use App\Models\File;

interface FileRepositoryInterface
{
    public const CREATE_RULES = [
        'title' => 'required',
        'encodedFile' => 'required',
        'description' => 'required',
        'file' => 'required'
    ];

    public const UPDATE_RULES = [
        'title' => 'required',
        'description' => 'required',
    ];

    /**
     * @param mixed[] $params
     *
     * @return \App\Models\File
     */
    public function create(array $params): File;

    /**
     * @param string $id
     *
     * @return null|\App\Models\File
     */
    public function getById(string $id): ?File;

    /**
     * @param mixed[] $params
     *
     * @return object
     */
    public function paginate(array $params): object;

    /**
     * @param string $id
     * @param array $params
     *
     * @return \App\Models\File
     */
    public function update(string $id, array $params): File;
}
