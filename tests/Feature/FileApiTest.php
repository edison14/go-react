<?php
declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class FileApiTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    public function tetList(): void
    {
        $response = $this->get('/api/files');

        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testCreateWithValidationErrors(): void
    {
        $params = [
            'title' => 'test'
        ];

        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/files', $params);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $params = [
            'title' => 'test',
            'description' => 'sample description',
            'encodedFile' => [
                'ext' => 'jpg',
                'encoded' => base64_encode('sample-base64-encoded-string')
            ],
            'file' => 'sample-file-name.jpg'
        ];

        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/files', $params);

        $response->assertStatus(200);
    }


    /**
     * @return void
     */
    public function testCreateWithInvalidExtension(): void
    {
        $params = [
            'title' => 'test',
            'description' => 'sample description',
            'encodedFile' => [
                'ext' => 'pdf',
                'encoded' => base64_encode('sample-base64-encoded-string')
            ],
            'file' => 'sample-file-name.jpg'
        ];

        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/files', $params);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testGetById(): void
    {
        $params = [
            'title' => 'test',
            'description' => 'sample description',
            'encodedFile' => [
                'ext' => 'jpg',
                'encoded' => base64_encode('sample-base64-encoded-string')
            ],
            'file' => 'sample-file-name.jpg'
        ];

        $result = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/files', $params);

        $file = json_decode($result->getContent());

        $response = $this->get(\sprintf('/api/file/%s', $file->id));

        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetByInvalidId(): void
    {
        $response = $this->get('/api/file/ random-id');

        $response->assertStatus(404);
    }
}
