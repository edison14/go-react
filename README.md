# Go React Backend
## A simple api to be used for uploading of JPG and MP4 files.

### Setup

#### Please install the following.
* Docker version 19.03.8
* Docker-compose version 1.25.5
* Composer version 1.9.0
* PHPunit

#### Running your local server.
* After cloning.
* Run composer install to install dependencies.
* Navigate to your `go-react` directory.
* Run `docker-compose up`.
* Run `cp .env.final .env`.
* Run `docker-compose exec app php artisan migrate` to run migrations for the tables.
* Wait a moment for it to install and load.
* Navigate to your localhost and you should see the welcome page.

NOTE: In Case theres a problem regarding network with docker please try to add these on your hosts file.
* 127.0.0.1	mysql
* 127.0.0.1	localhost db

#### Testing
* Run `php artisan test`

#### Code Coverage
* Install xdebug as a driver. `pecl install xdebug`
* Run `./vendor/bin/phpunit --coverage-html reports/`
* Navigate to reports directory and view dashboard.html on a browser.




