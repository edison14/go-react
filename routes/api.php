<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('files', 'FileController@create');
Route::get('files', 'FileController@get');
Route::get('file/{id}', 'FileController@view');
Route::patch('files/{id}', 'FileController@update');

